<?php

// Route::get('/', 'TodoListController@index');
// Route::get('/todos', 'TodoListController@index');
// Route::get('/todos/{id}', 'TodoListController@show');

// Route::get('/db', function(){
// 	// return DB::select('show tables;');
// 	// return DB::table('todo_lists')->get();

// 	// DB::table('todo_lists')->insert(array(
// 	// 	'name' => 'Max',
// 	// 	'created_at' => time(),
// 	// 	'updated_at' => time()
// 	// 	));

// 	// $result = DB::table('todo_lists')->where('name', 'Ionut')->first();
// 	// return $result->id;
// 	return DB::table('todo_lists')->get();
// });


// ce incarca pe localhost/ controller action
Route::get('/', 'BlogController@index');


// Route::get('/', function(){
// 	$a = User::all();
// 	foreach($a as $b)
// 	{
// 		echo $b->email.'<br>';
// 		echo $b->username.'<br>';
// 		echo $b->password.'<br>';
// 		echo $b->remember_token.'<br><br><br><br><br>';
// 	}
// });


// incarca resursele din blog controller, folderu blog
Route::resource('blog', 'BlogController');


Route::get('/signup', 'AccountController@signup');
Route::post('/signupPost', 'AccountController@signupPost');
Route::get('/login', 'AccountController@login');
Route::post('/loginPost', 'AccountController@loginPost');
Route::get('/logout', 'AccountController@logout');
Route::get('/info', 'AccountController@info');