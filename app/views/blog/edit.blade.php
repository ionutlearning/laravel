@extends('layouts.blog')
@section('content')
	<h1>Edit post</h1>
	<!-- form model (arrayu din db, array(ruta -> array(href, id), metoda => put))	put - update -->
	<!-- populeaza forma, dupa modificare o trmite pe actionu update ca sa faca modificarile -->
	{{	Form::model($article, array('route' => ['blog.update', $article->id], 'method' => 'put'))}}
	<div class="form-group">

		<!-- form label ( for, value ) -->
		{{	Form::label('title', 'Titlu articol')	}}
		<!-- form text ( name, value, clasa) -->
		<!-- daca name e la fel cu cel de la label pune si id -->
		{{	Form::text('title', null, array('class' => 'form-control'))	}}
	</div>
	<div class="form-group">
		{{	Form::label('content', 'Continut articol')	}}
		{{	Form::textarea('content', null, array('class' => 'form-control'))	}}
	</div>
	
	<!-- $errors->has() si $errors->all() -->
	@if($errors->has())
		@foreach($errors->all() as $error)
			<div class="alert alert-danger">{{{ $error }}}</div>
		@endforeach
	@endif

		<!-- form submit (value, atribute) -->
		{{	Form::submit('Editeaza', array('class' => 'btn btn-warning'))	}}

	<!-- form close() -->
	{{ Form::close() }}
@stop