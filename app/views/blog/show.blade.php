@extends('layouts.blog')
@section('content')

	<!-- link (href, content, parametru, atribute) -->
	{{ link_to_route('blog.index', '', null, array('class' => 'glyphicon glyphicon-arrow-left'))  }}

    <!-- cand e doar un row   $row->camp -->
    <h1>{{{$article->title}}}</h1>
    <p class="lead">by {{{$article->author}}}</p>
    <p><span class="glyphicon glyphicon-time"></span> Posted on {{{$article->created_at->format('H:i:s d-m-Y')}}}</p>
    <p>{{{$article->content}}}</p>

    <hr>
    <ul class="list-inline">
	  <li>
	  	<!-- link (href, conent, parametru)  /blog/id/edit   -->
    	{{  link_to_route('blog.edit', 'editeaza', array($article->id), array('class' => 'btn btn-xs btn-success')) }}
	  </li>
	  <li>
	  	<!-- form model (array din db, array(ruta=> array(locatie, id) metoda => delete) )   delete = destroy -->
	    {{	Form::model($article, ['route' => ['blog.destroy', $article->id], 'method' => 'delete'])	}}
	    <!-- button ( nume, array(type, atribute)) -->
	    {{	Form::button('delete', ['type' => 'submit', 'class' => 'btn btn-xs btn-danger'])}}
	    {{	Form::close()	}}
	  </li>
	</ul>
@stop