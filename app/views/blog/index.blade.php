@extends('layouts.blog')
@section('content')

	<!-- trimite pe toate pg, $message -->
	@if(Session::has('message'))
	     <div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">×</button>
	        {{{	Session::get('message')	}}}
	     </div>
	@elseif(Session::has('warning'))
	     <div class="alert alert-warning"><button class="close" data-dismiss="alert" type="button">×</button>
	        {{{	Session::get('warning')	}}}
	     </div>
	@endif

	<div class="row">
        <div class="col-lg-8">
           <h1>{{{$articles->first()->title}}}</h1>
            <p class="lead">by {{{$articles->first()->author}}}</p>
            <p><span class="glyphicon glyphicon-time"></span> Posted on {{{$articles->first()->created_at->format('H:i:s d-m-Y')}}}</p>
            <hr>
            <p class="lead">{{{$articles->first()->content}}}</p>
        </div>
         <div class="col-lg-4">
			 <div class="well">
	            <h4>Ultimele articole</h4>
	            <div class="row">
	                <div class="col-lg-6">
	                    <ul class="list-unstyled">
	                        @foreach($articles as $article)
						    	<!-- link route (href, nume, id ) /href/nume/id -->
						     	<li>{{ link_to_route('blog.show', $article->title, array($article->id))	}}</li>
						    @endforeach
	                    </ul>
	                </div>
	            </div>
	        </div>
		</div>
	</div>
@stop