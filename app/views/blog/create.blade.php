@extends('layouts.blog')
@section('content')
	<h1>Add a new post</h1>
	<!-- form open -> action -->
	{{ Form::open(array( 'route' => 'blog.store')) }}
	<div class="form-group">

		<!-- form label ( for, value ) -->
		{{	Form::label('title', 'Titlu articol')	}}
		<!-- form text ( name, value, clasa) -->
		<!-- daca name e la fel cu cel de la label pune si id -->
		{{	Form::text('title', null, array('class' => 'form-control'))	}}
	</div>
	<div class="form-group">
		{{	Form::label('content', 'Continut articol')	}}
		{{	Form::textarea('content', null, array('class' => 'form-control'))	}}
	</div>
	
	<!-- $errors->has() si $errors->all() -->
	@if($errors->has())
		@foreach($errors->all() as $error)
			<div class="alert alert-danger"><button class="close" data-dismiss="alert" type="button">×</button>{{{ $error }}}</div>
		@endforeach
	@endif

		<!-- form submit (value, atribute) -->
		{{	Form::submit('Adauga', array('class' => 'btn btn-success'))	}}

	<!-- form close() -->
	{{ Form::close() }}
@stop