<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Blog</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
	<style>body{padding-top:100px;}</style>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- link to route (	nume routa, nume link, null, clasa) -->
                {{ link_to_route('blog.index', 'My Blog', null, array('class' => 'navbar-brand')) }}
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        {{  link_to_route('blog.create', 'Add article') }}
                    </li><li>
                        {{  link_to('signup', 'Register') }}
                    </li><li>
                        {{  link_to('login', 'Login') }}
                    </li><li>
                        {{  link_to('info', 'Secret page') }}
                    </li><li>
                        {{  link_to('logout', 'Logout') }}
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
    	@yield('content')
    </div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  	{{ HTML::script('js/bootstrap.min.js') }}
	<!-- {{ HTML::script('js/app.js') }} -->
</body>
</html>