@extends('layouts.blog')
@section('content')
<div class="row">
	<div class="col-lg-4">
		<h1>Secret page</h1>
        <ul class="list-group">
            <li class="list-group-item text-muted" contenteditable="false">Profile</li>
            <li class="list-group-item text-right"><span class="pull-left"><strong class="">Username</strong></span> {{	$info->username }}</li>
            <li class="list-group-item text-right"><span class="pull-left"><strong class="">Email</strong></span> {{	$info->email }}</li>
            <li class="list-group-item text-right"><span class="pull-left"><strong class="">Joined</strong></span> {{	$info->created_at->format('H:i:s d-m-Y') }}</li>
        </ul>
	</div>
</div>
@stop