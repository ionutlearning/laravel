@extends('layouts.blog')
@section('content')
<div class="row">
	<div class="col-lg-6">

		@if(Session::has('message'))
		     <div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">×</button>
		        {{{	Session::get('message')	}}}
		     </div>
		@endif

		<h1>Log in page</h1>
		{{	Form::open(array('url' => '/loginPost'))	}}
		<!-- <div class="form-group">
			{{	Form::label('username', 'Username')	}}
			{{	Form::text('username', null, ['class' => 'form-control'])	}}
		</div> -->	
		<div class="form-group">
			{{	Form::label('email', 'Email')	}}
			{{	Form::text('email', null, ['class' => 'form-control'])	}}
		</div>	
		<div class="form-group">
			{{	Form::label('password', 'Password')	}}
			{{	Form::password('password', ['class' => 'form-control'])	}}
		</div>

		@if($errors->has())
		@foreach($errors->all() as $error)
			<div class="alert alert-danger"><button class="close" data-dismiss="alert" type="button">×</button>{{{ $error }}}</div>
			@endforeach
		@endif

		<div class="form-group">
			{{	Form::submit('Log in', ['class' => 'btn btn-success'])	}}
		</div>
		{{	Form::close()	}}
		{{	link_to('/signup', 'create account')	}}
	</div>
</div>
@stop