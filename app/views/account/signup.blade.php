@extends('layouts.blog')
@section('content')
<div class="row">
	<div class="col-lg-6">
		<h1>Sign up page</h1>
		{{	Form::open(array('url' => '/signupPost'))	}}
		<div class="form-group">
			{{	Form::label('username', 'Username')	}}
			{{	Form::text('username', null, ['class' => 'form-control'])	}}
		</div>	
		<div class="form-group">
			{{	Form::label('email', 'Email')	}}
			{{	Form::text('email', null, ['class' => 'form-control'])	}}
		</div>	
		<div class="form-group">
			{{	Form::label('password', 'Password')	}}
			{{	Form::password('password', ['class' => 'form-control'])	}}
		</div>

		@if($errors->has())
		@foreach($errors->all() as $error)
			<div class="alert alert-danger"><button class="close" data-dismiss="alert" type="button">×</button>{{{ $error }}}</div>
			@endforeach
		@endif

		<div class="form-group">
			{{	Form::submit('Register', ['class' => 'btn btn-success'])	}}
		</div>
		{{	Form::close()	}}
		{{	link_to('/login', 'enter site')	}}
	</div>
</div>
@stop