<?php

class AccountController extends BaseController{

	public function __construct()
	{
		$this->beforeFilter('csrf', array('on' => ['post']));

		//	require login
        $this->beforeFilter('auth', array('only' =>
                            array('info')));
	}
	
	public function login(){

		//	update db
		// Schema::table('blog_articles', function($new)
		// {
		// 	$new->string('author');
		// });

		if(Auth::check())
		{
			return Redirect::to('/')->withWarning('You are already logged in!');
		}

		return View::make('account.login');
	}

	public function signup(){

		if(Auth::check())
		{
			return Redirect::to('/')->withWarning('You are already logged in!');
		}
		return View::make('account.signup');
	}

	public function signupPost()
	{
		$rules = array(
			'username' => array('required', 'unique:users'),
			'email' => array('required', 'email', 'unique:users'),
			'password' => array('required', 'min:6')
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails())
		{
			return Redirect::to('/signup')->withErrors($validator)->withInput();
		}else{
			//	daca datele sunt valide se salveaza si db si redirect
			$new = new User;
			$new->username = Input::get('username');
			$new->email = Input::get('email');
			$new->password = Hash::make(Input::get('password'));
			$new->save();

			return Redirect::to('/login')->withMessage('Account created!');
		}
	}

	public function loginPost()
	{
		$rules = array(
			// 'username' => array('required'),
			'email' => array('required', 'email'),
			'password' => array('required')
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails())
		{
			return Redirect::to('/login')->withErrors($validator)->withInput();
		}else{
			//	login dupa email si passord
			$credentials = Input::only('email', 'password');

			//	daca sunt corecte
			if(Auth::attempt($credentials))
			{
				return Redirect::to('/');
			}

			return Redirect::to('/login')->withErrors('Wrong credentials');
		}
	}

	public function logout(){
		//	logout
		Auth::logout();
		return View::make('account.logout');
	}

	public function info(){
		//	datele userului logat
		$info = Auth::user();
		return View::make('account.info')->withInfo($info);
	}
}