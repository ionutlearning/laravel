<?php

class BlogController extends \BaseController {

	// pt securiate, tokenu de pe input, sa nu se schimbe la edit
	public function __construct()
	{
		$this->beforeFilter('csrf', array('on' => ['post', 'put']));

		//	require login
		// $this->beforeFilter('auth');
        $this->beforeFilter('auth', array('only' =>
                            array('create', 'edit', 'destroy')));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// select *
		// $articles = BlogArticle::all();
		$articles = BlogArticle::orderBy('id', 'DESC')->get();
		return View::make('blog.index')->withArticles($articles);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return View::make('blog.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// reguli  -> array (camp => validari)
		// in cazul in care camp e diferit de cel din baza de date: unique:TABELA, NUME DIN TABELA
		$rules = array(
			'title' => array('required', 'unique:blog_articles'),
			'content' => array('required')
		);

		// aplica validare pe toate inputurile
		$validator = Validator::make(Input::all(), $rules);

		// redirect daca datele nu sunt valide, cu tot cu erori si numele inputurilor
		if($validator->fails())
		{
			return Redirect::route('blog.create')->withErrors($validator)->withInput();
		}

		// ce vine pe inputu title
		$title = Input::get('title');
		$content = Input::get('content');

		// model setez campurile si save
		$model = new BlogArticle();
		$model->title = $title;
		$model->content = $content;
		$model->author = Auth::user()->username;
		$model->save();

		return Redirect::route('blog.index')->withMessage('Articol adaugat!');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// select dupa id
		$article = BlogArticle::findOrFail($id);

		// incarc blade view
		return View::make('blog.show')->withArticle($article);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$article = BlogArticle::findOrFail($id);
		return View::make('blog.edit')->withArticle($article);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'title' => array('required', 'unique:blog_articles'),
			'content' => array('required')
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails())
		{
			return Redirect::route('blog.edit', $id)->withErrors($validator)->withInput();
		}

		$title = Input::get('title');
		$content = Input::get('content');

		// cauta row dupa id
		$model = BlogArticle::findOrFail($id);
		$model->title = $title;
		$model->content = $content;
		$model->update();

		return Redirect::route('blog.index')->withMessage('Updated!');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$destroy = BlogArticle::findOrFail($id)->delete();
		return Redirect::route('blog.index')->withMessage('Deleted!');
	}


}
